import 'package:conex/team/screens/teams_home_screen/teams_home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'auth/provider/_.dart';
import 'auth/screens/login_screen/login_screen.dart';
import 'common/widgets/_.dart';

class HomeScreen extends ConsumerWidget {
  const HomeScreen({Key? key}) : super(key: key);

  static Route route() => MaterialPageRoute(
        builder: (_) => const HomeScreen(),
      );

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = ref.watch(loggedUserProvider);

    return state.when(
      data: (data) =>
          data == null ? const LoginScreen() : const TeamsHomeScreen(),
      error: (error, stackTrace) => const LoginScreen(),
      loading: () => const Loading(),
    );
  }
}
