import 'package:conex/auth/provider/_.dart';
import 'package:conex/common/utils/navigation_utils.dart';
import 'package:conex/common/widgets/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  static Route route() => MaterialPageRoute(
        builder: (_) => const SettingsScreen(),
      );

  @override
  Widget build(BuildContext context) {
    return ScreenContainer(
      child: _SettingsList(),
      centered: true,
    );
  }
}

class _SettingsList extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<AuthState>(authProvider, (previous, next) {
      next.effect(onSuccess: (success) => homeRoute.goTo(context));
    });
    final authNotifier = ref.read(authProvider.notifier);

    return AppColumn(
      children: [
        Btn(
          onPressed: authNotifier.logOut,
          child: const Text('Iesi din cont'),
        ),
      ],
    );
  }
}
