import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';

abstract class StorageService {
  Future<void> postFile(File file, String uploadPath);
  Future<String> getDownloadUrl(String filePath);
  Future<void> deleteFile(String filePath);
}

class FBaseStorageService extends StorageService {
  final FirebaseStorage _firebaseStorage;

  FBaseStorageService() : _firebaseStorage = FirebaseStorage.instance;

  @override
  Future<void> postFile(File file, String uploadPath) async {
    await _firebaseStorage.ref(uploadPath).putFile(file);
  }

  @override
  Future<String> getDownloadUrl(String filePath) async {
    final ref = _firebaseStorage.ref(filePath);
    return ref.getDownloadURL();
  }

  @override
  Future<void> deleteFile(String filePath) async {
    final ref = _firebaseStorage.ref(filePath);
    await ref.delete();
  }
}
