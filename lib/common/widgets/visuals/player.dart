import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../sizes.dart';
import '../../theme.dart';
import '../_.dart';

class PlayerDisplay extends StatelessWidget {
  final bool withMargin;
  final bool filled;
  final String assetLocation;
  final double width;
  final double height;

  const PlayerDisplay(
      {this.filled = false,
      this.withMargin = false,
      this.width = 16,
      this.height = 16,
      Key? key})
      : assetLocation =
            filled ? 'assets/svg/player.svg' : 'assets/svg/nofill_player.svg',
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: withMargin
          ? const EdgeInsets.symmetric(horizontal: Spacing.xxs)
          : EdgeInsets.zero,
      child: SvgPicture.asset(
        assetLocation,
        color: appSwatch[300],
        width: width,
        height: height,
      ),
    );
  }
}

class PlayersDisplay extends StatelessWidget {
  final EdgeInsets margin;
  final int filledNumberOfPlayers;
  final int totalNumberOfPlayers;
  final double width;
  final double height;

  const PlayersDisplay(
    this.filledNumberOfPlayers,
    this.totalNumberOfPlayers, {
    this.margin = EdgeInsets.zero,
    this.width = 16,
    this.height = 16,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<PlayerDisplay> players = [];
    for (int i = 0; i < totalNumberOfPlayers; i++) {
      players.add(PlayerDisplay(
        filled: i < filledNumberOfPlayers,
        withMargin: i != 0 || i != totalNumberOfPlayers,
        width: width,
        height: height,
      ));
    }
    return AppRow(
      margin: margin,
      children: players,
    );
  }
}
