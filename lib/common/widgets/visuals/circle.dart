import 'package:flutter/material.dart';

class Circle extends StatelessWidget {
  final Color? color;

  const Circle({this.color, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 16,
      height: 16,
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
    );
  }
}

class SeverityDisplay extends StatelessWidget {
  final String severity;

  const SeverityDisplay(this.severity, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (severity == 'low') {
      return const Circle(color: Colors.green);
    }
    if (severity == 'medium') {
      return const Circle(color: Colors.orange);
    }
    if (severity == 'high') {
      return const Circle(color: Colors.red);
    }
    return const Circle(color: Colors.green);
  }
}
