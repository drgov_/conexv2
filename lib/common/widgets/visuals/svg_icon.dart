import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SvgIcon extends StatelessWidget {
  final String svg;

  const SvgIcon(this.svg, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      'assets/svg/$svg.svg',
      width: 32,
      height: 32,
      color: Colors.white,
    );
  }
}
