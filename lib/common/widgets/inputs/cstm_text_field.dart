import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CstmTextField extends StatelessWidget {
  final EdgeInsets margin;
  final TextEditingController controller;
  final bool obscureText;
  final String? hintText;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;

  const CstmTextField({
    this.margin = EdgeInsets.zero,
    required this.controller,
    this.obscureText = false,
    this.hintText,
    this.keyboardType,
    this.inputFormatters,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: TextField(
        controller: controller,
        obscureText: obscureText,
        decoration: InputDecoration(
          hintText: hintText,
        ),
        keyboardType: keyboardType,
        inputFormatters: inputFormatters,
      ),
    );
  }
}

class HiddenField extends CstmTextField {
  const HiddenField({
    EdgeInsets margin = EdgeInsets.zero,
    required TextEditingController controller,
    String? hintText,
    Key? key,
  }) : super(
          margin: margin,
          controller: controller,
          obscureText: true,
          hintText: hintText,
          key: key,
        );
}

class NumberTextField extends CstmTextField {
  NumberTextField({
    EdgeInsets margin = EdgeInsets.zero,
    required TextEditingController controller,
    String? hintText,
    Key? key,
  }) : super(
          margin: margin,
          controller: controller,
          hintText: hintText,
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          key: key,
        );
}
