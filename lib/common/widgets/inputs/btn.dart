import 'package:flutter/material.dart';

class Btn extends StatelessWidget {
  final EdgeInsets margin;
  final void Function()? onPressed;
  final Widget? child;

  const Btn({
    this.margin = EdgeInsets.zero,
    this.onPressed,
    this.child,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: ElevatedButton(
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}
