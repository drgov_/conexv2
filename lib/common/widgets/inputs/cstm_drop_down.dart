import 'package:flutter/material.dart';

class CstmDropDownField<T> extends StatelessWidget {
  final EdgeInsets margin;
  final T? value;
  final void Function(T?)? onChanged;
  final List<T> items;
  final Widget Function(T) item;
  final String hintText;

  const CstmDropDownField({
    this.value,
    this.margin = EdgeInsets.zero,
    this.onChanged,
    this.items = const [],
    this.hintText = '',
    required this.item,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: DropdownButtonFormField<T>(
        value: value,
        items: items
            .map((e) => DropdownMenuItem<T>(
                  child: item(e),
                  value: e,
                ))
            .toList(),
        onChanged: onChanged,
        onTap: () {
          FocusManager.instance.primaryFocus!.unfocus();
        },
        decoration: InputDecoration(
          hintText: hintText,
        ),
      ),
    );
  }
}
