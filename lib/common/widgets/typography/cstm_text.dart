import 'package:flutter/material.dart';

class CstmText extends StatelessWidget {
  final String data;
  final TextStyle? style;
  final EdgeInsets margin;

  const CstmText(
    this.data, {
    this.margin = EdgeInsets.zero,
    this.style,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: Text(
        data,
        style: style,
      ),
    );
  }
}
