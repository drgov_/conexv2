import 'package:flutter/material.dart';

import '../../sizes.dart';
import '../../theme.dart';
import 'cstm_text.dart';

class Header extends CstmText {
  final double fontSize;
  final Color? color;
  final bool bold;

  Header(
    String data, {
    this.fontSize = CstmFontSize.sh,
    this.color,
    this.bold = false,
    EdgeInsets margin = EdgeInsets.zero,
    Key? key,
  }) : super(
          data,
          style: TextStyle(
            fontSize: fontSize,
            fontWeight: bold ? FontWeight.bold : FontWeight.normal,
            overflow: TextOverflow.ellipsis,
            color: color,
          ),
          margin: margin,
          key: key,
        );
}

class AccentHeader extends Header {
  AccentHeader(
    String data, {
    double fontSize = CstmFontSize.sh,
    bool bold = false,
    EdgeInsets margin = EdgeInsets.zero,
    Key? key,
  }) : super(
          data,
          fontSize: fontSize,
          color: appSwatch[100],
          bold: bold,
          margin: margin,
          key: key,
        );
}
