import 'package:conex/common/utils/navigation_utils.dart';
import 'package:conex/common/widgets/visuals/svg_icon.dart';
import 'package:flutter/material.dart';

import '../../sizes.dart';

class CstmContainer extends StatelessWidget {
  final EdgeInsets margin;
  final EdgeInsets padding;
  final Widget? child;

  const CstmContainer({
    this.margin = EdgeInsets.zero,
    this.padding = EdgeInsets.zero,
    this.child,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: Container(
        child: Padding(
          padding: padding,
          child: child,
        ),
      ),
    );
  }
}

class FloatingNav extends StatelessWidget {
  final List<AppRoute> routes;

  const FloatingNav({required this.routes, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: routes
          .map(
            (route) => Padding(
              padding: const EdgeInsets.symmetric(vertical: Spacing.s),
              child: FloatingActionButton(
                onPressed: () => route.goTo(context),
                child: SvgIcon(route.icon),
                heroTag: route.path,
              ),
            ),
          )
          .toList(),
    );
  }
}
