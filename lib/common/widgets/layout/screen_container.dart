import 'package:flutter/material.dart';

import '../../sizes.dart';
import 'cstm_container.dart';

class ScreenContainer extends StatelessWidget {
  final Widget? child;
  final Widget? floatingContent;
  final bool centered;

  const ScreenContainer({
    this.child,
    this.floatingContent,
    this.centered = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (centered) {
      return Scaffold(
        body: Center(
          child: _ChildScrollView(
            child: child,
          ),
        ),
        floatingActionButton: floatingContent,
      );
    }
    return Scaffold(
      body: _ChildScrollView(
        child: child,
      ),
      floatingActionButton: floatingContent,
    );
  }
}

class _ChildScrollView extends StatelessWidget {
  final Widget? child;

  const _ChildScrollView({this.child, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: CstmContainer(
        margin: const EdgeInsets.symmetric(
          horizontal: Spacing.l,
          vertical: Spacing.xxl,
        ),
        child: child,
      ),
    );
  }
}
