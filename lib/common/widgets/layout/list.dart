import 'package:conex/common/sizes.dart';
import 'package:flutter/material.dart';

import '../../theme.dart';

class AppCard extends StatelessWidget {
  final Widget? child;
  final void Function()? onTap;
  final EdgeInsets margin;

  const AppCard({
    this.child,
    this.onTap,
    this.margin = EdgeInsets.zero,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margin,
      child: Material(
        borderRadius: BorderRadius.circular(16),
        color: appSwatch.withOpacity(0.075),
        child: InkWell(
          borderRadius: BorderRadius.circular(16),
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: Spacing.m,
              horizontal: Spacing.m,
            ),
            child: child,
          ),
        ),
      ),
    );
  }
}

class AppList<T> extends StatelessWidget {
  final List<T> data;
  final Widget Function(T) item;
  final Widget whenEmpty;
  const AppList({
    this.data = const [],
    required this.item,
    Key? key,
    this.whenEmpty = const Text('No items'),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return data.isEmpty
        ? whenEmpty
        : ListView(
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            children: data.map((e) => item(e)).toList(),
          );
  }
}
