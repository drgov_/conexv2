class Spacing {
  static const double spaceUnit = 16;
  static const double xxs = 0.25 * spaceUnit;
  static const double xs = 0.5 * spaceUnit;
  static const double s = 0.75 * spaceUnit;
  static const double m = 1.25 * spaceUnit;
  static const double l = 2 * spaceUnit;
  static const double xl = 3.25 * spaceUnit;
  static const double xxl = 4.5 * spaceUnit;
}

class CstmFontSize {
  static const double fontSizeUnit = 14;
  static const double xsh = 1.25 * fontSizeUnit;
  static const double sh = 1.5 * fontSizeUnit;
  static const double mh = 2 * fontSizeUnit;
  static const double lh = 2.5 * fontSizeUnit;
  static const double xlh = 3.5 * fontSizeUnit;
}
