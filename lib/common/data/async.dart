import 'package:conex/common/data/_.dart';

abstract class AsyncState {
  const AsyncState();

  T returnOn<T>({
    T Function()? onInit,
    T Function()? onLoading,
    T Function(AppSuccess)? onSuccess,
    T Function(AppError)? onError,
    required T Function() onElse,
  }) {
    if (onInit != null && this is InitState) {
      return onInit();
    }
    if (onLoading != null && this is LoadingState) {
      return onLoading();
    }
    if (onSuccess != null && this is SuccessState) {
      return onSuccess((this as SuccessState).success);
    }
    if (onError != null && this is ErrorState) {
      return onError((this as ErrorState).error);
    }
    return onElse();
  }

  void effect({
    void Function()? onInit,
    void Function()? onLoading,
    void Function(AppSuccess)? onSuccess,
    void Function(AppError)? onError,
    void Function()? onAny,
  }) {
    if (onInit != null && this is InitState) {
      onInit();
    }
    if (onLoading != null && this is LoadingState) {
      onLoading();
    }
    if (onSuccess != null && this is SuccessState) {
      onSuccess((this as SuccessState).success);
    }
    if (onError != null && this is ErrorState) {
      onError((this as ErrorState).error);
    }
    if (onAny != null) {
      onAny();
    }
  }
}

abstract class InitState extends AsyncState {
  const InitState();
}

abstract class LoadingState extends AsyncState {
  const LoadingState();
}

abstract class SuccessState extends AsyncState {
  final AppSuccess success;

  const SuccessState([this.success = const AppSuccess()]);
}

abstract class ErrorState extends AsyncState {
  final AppError error;

  const ErrorState(this.error);
}
