enum ERROR_TYPE { invalid, notFound, conflict, other }

class AppError {
  final String _message;
  final ERROR_TYPE _type;

  AppError(this._type, this._message);

  String get message => _message;
  ERROR_TYPE get type => _type;
}

class InvalidError extends AppError {
  InvalidError(String message) : super(ERROR_TYPE.invalid, message);
}

class NotFoundError extends AppError {
  NotFoundError(String message) : super(ERROR_TYPE.notFound, message);
}

class ConflictError extends AppError {
  ConflictError(String message) : super(ERROR_TYPE.conflict, message);
}

class OtherError extends AppError {
  OtherError(String message) : super(ERROR_TYPE.other, message);
}
