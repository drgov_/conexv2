import '../../game/data/game.dart';
import '../../team/data/team.dart';
import 'app_error.dart';

class Validator {
  static bool empty(String? string) => string == null || string.isEmpty;
  static bool notNumber(String string) => int.tryParse(string) == null;
  static bool isNull(dynamic data) => data == null;

  static void validateLoginData(String email, String password) {
    if (empty(email)) {
      throw InvalidError('Email-ul nu e completat');
    }
    if (empty(password)) {
      throw InvalidError('Parola nu e completata');
    }
  }

  static void validateRegisterData(
    String email,
    String username,
    String password,
  ) {
    if (empty(email)) {
      throw InvalidError('Email-ul nu e completat');
    }
    if (empty(username)) {
      throw InvalidError('Username-ul nu e completat');
    }
    if (empty(password)) {
      throw InvalidError('Parola nu e completata');
    }
  }

  static void validateGameData(Game game) {
    if (empty(game.name)) {
      throw InvalidError('Nume joc nu e completat');
    }
    if (empty(game.maxTeam)) {
      throw InvalidError('Numar jucatori nu e completat');
    }
    if (notNumber(game.maxTeam)) {
      throw InvalidError('Numar maxim jucatori in echipa nu e numar');
    }
  }

  static void validateTeamData(Team team) {
    if (empty(team.name)) {
      throw InvalidError('Nume echipa nu e completat');
    }
    if (empty(team.neededPlayers)) {
      throw InvalidError('Numar jucatori nu e completat');
    }
    if (notNumber(team.neededPlayers)) {
      throw InvalidError('Numar jucatori nu e numar');
    }
    if (empty(team.gameId)) {
      throw InvalidError('Nu ai ales niciun joc');
    }
  }
}
