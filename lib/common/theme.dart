import 'package:conex/common/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

const appSwatch = MaterialColor(
  0xff8567fe,
  <int, Color>{
    50: Color(0xfff3f0ff),
    100: Color(0xffcec2ff),
    200: Color(0xffaa95fe),
    300: Color(0xff9176fe),
    400: Color(0xff8567fe),
    500: Color(0xff8567fe),
    600: Color(0xff5d48b2),
    700: Color(0xff352966),
    800: Color(0xff1b1533),
    900: Color(0xff0d0a19),
  },
);
const errorColor = Color(0xffff3859);
const successColor = Color(0xff00bb66);

final _darkThemeData = ThemeData(
  brightness: Brightness.dark,
  primaryColor: appSwatch[50],
  primarySwatch: appSwatch,
  scaffoldBackgroundColor: appSwatch[900],
  canvasColor: appSwatch[800],
  inputDecorationTheme: InputDecorationTheme(
    border: OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.circular(2.5),
    ),
    filled: true,
    fillColor: appSwatch[800]!.withOpacity(0.8),
    hintStyle: TextStyle(
      color: appSwatch[100],
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: const BorderSide(
        color: appSwatch,
        width: 2.0,
      ),
      borderRadius: BorderRadius.circular(2.5),
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: appSwatch,
      minimumSize: const Size(160, 45),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18),
      ),
      textStyle: const TextStyle(
        fontSize: CstmFontSize.xsh,
        fontWeight: FontWeight.bold,
        color: Colors.white,
      ),
    ),
  ),
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: appSwatch[600],
    extendedTextStyle: const TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.bold,
    ),
    foregroundColor: Colors.white,
  ),
  snackBarTheme: const SnackBarThemeData(
    backgroundColor: appSwatch,
    contentTextStyle: TextStyle(
      color: Colors.white,
    ),
  ),
);

class AppTheme {
  static final darkTheme = _darkThemeData;
}

class ThemeState {
  final ThemeData themeData = AppTheme.darkTheme;
}

final themeProvider = Provider((ref) => ThemeState());
