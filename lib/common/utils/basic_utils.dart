import 'dart:math';

import 'package:conex/auth/data/app_user.dart';
import 'package:conex/team/data/team.dart';
import 'package:sensors_plus/sensors_plus.dart';

String replaceSpacesWithUnderscore(String string) =>
    string.replaceAll(RegExp('\\s+'), '_');

bool isUserInTeam(AppUser user, Team team) {
  return team.usersIds.any((element) => element == user.id);
}

bool isUsersTeam(AppUser user, Team team) {
  return user.id == team.userId;
}

double getAccel(UserAccelerometerEvent event) {
  final x = event.x;
  final y = event.y;
  final z = event.z;
  return sqrt(x * x + y * y + z * z);
}
