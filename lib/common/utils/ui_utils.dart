import 'package:conex/common/data/app_error.dart';
import 'package:flutter/material.dart';

import '../theme.dart';

void disposeControllers(List<TextEditingController> controllers) {
  for (var controller in controllers) {
    controller.dispose();
  }
}

void clearControllers(List<TextEditingController> controllers) {
  for (var controller in controllers) {
    controller.clear();
  }
}

void _showSnackBar(
  BuildContext context,
  String message,
  Color color,
) {
  if (message.isNotEmpty) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          backgroundColor: color,
          content: Text(message),
        ),
      );
  }
}

void showErrorSnackBar(BuildContext context, String message) {
  _showSnackBar(context, message, errorColor);
}

void showSuccessSnackBar(BuildContext context, String message) {
  _showSnackBar(context, message, successColor);
}

void Function(Object, StackTrace?) errorSnackBarEffect(BuildContext context) =>
    (Object error, StackTrace? stackTrace) {
      if (error is AppError) {
        showErrorSnackBar(context, error.message);
      }
    };
