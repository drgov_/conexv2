import 'package:conex/game/screens/add_game/add_game_screen.dart';
import 'package:conex/game/screens/games_home/games_screen.dart';
import 'package:conex/home_screen.dart';
import 'package:conex/settings/screens/settings_screen.dart';
import 'package:conex/team/screens/team_details/team_details_screen.dart';
import 'package:flutter/material.dart';

import '../../auth/screens/register_screen/register_screen.dart';
import '../../team/screens/add_team/add_team_screen.dart';

class AppRoute {
  final String path;
  final Route Function() route;
  final String icon;
  final bool clearHistory;

  const AppRoute({
    required this.path,
    required this.route,
    this.icon = '',
    this.clearHistory = false,
  });

  void goTo(BuildContext context, {bool clearHistory = false}) {
    if (clearHistory || this.clearHistory) {
      Navigator.of(context).pushAndRemoveUntil(route(), (route) => false);
    } else {
      Navigator.of(context).push(route());
    }
  }
}

final homeRoute = AppRoute(
  path: '/',
  route: () => HomeScreen.route(),
  clearHistory: true,
);

final settingsRoute = AppRoute(
  path: '/settings',
  route: () => SettingsScreen.route(),
  icon: 'settings',
);

final loginRoute = AppRoute(
  path: '/login',
  route: () => HomeScreen.route(),
  icon: 'login',
  clearHistory: true,
);

final registerRoute = AppRoute(
  path: '/register',
  route: () => RegisterScreen.route(),
  icon: 'register',
);

final teamsHomeRoute = AppRoute(
  path: '/teams',
  route: () => HomeScreen.route(),
  clearHistory: true,
);

AppRoute teamDetailsRoute(String teamId) => AppRoute(
      path: '/teams/$teamId',
      route: () => TeamDetailsScreen.route(teamId),
    );

final addTeamRoute = AppRoute(
  path: '/add-team',
  route: () => AddTeamScreen.route(),
  icon: 'team',
);

final gamesHomeRoute = AppRoute(
  path: '/games',
  route: () => GamesHomeScreen.route(),
  icon: 'gamepad',
);

final addGameRoute = AppRoute(
  path: '/add-game',
  route: () => AddGameScreen.route(),
  icon: 'add_game',
);

void popPage(BuildContext context) {
  Navigator.of(context).pop();
}
