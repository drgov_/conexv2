import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:conex/common/data/app_error.dart';

import 'database_service.dart';
import 'serializable.dart';

class FStoreService<T extends Serializable> extends DatabaseService<T> {
  final CollectionReference _collectionRef;

  FStoreService(FirebaseFirestore instance, String collectionName)
      : _collectionRef = instance.collection(collectionName);

  @override
  Future<String> add(T data) async {
    await _collectionRef.doc(data.id).set(data.toMap());
    return data.id;
  }

  @override
  Future<void> deleteById(String id) async {
    await _collectionRef.doc(id).delete();
  }

  @override
  Future<void> update(String id, Map<String, Object?> data) async {
    await _collectionRef.doc(id).update(data);
  }

  @override
  Future<void> deleteBy(List<Where> queries) async {
    Query collectionQuery = _collectionRef;
    for (var query in queries) {
      if (query.isEqualTo) {
        collectionQuery = collectionQuery.where(
          query.fieldName,
          isEqualTo: query.fieldValue,
        );
      }
      if (query.isArrayContains) {
        collectionQuery = collectionQuery.where(
          query.fieldValue,
          arrayContains: query.fieldValue,
        );
      }
    }
    final qShot = await collectionQuery.get();
    for (final doc in qShot.docs) {
      await doc.reference.delete();
    }
  }

  @override
  Stream<List<T>> getAll() {
    return _collectionRef.snapshots().map(
          (event) => event.docs.isEmpty
              ? []
              : event.docs
                  .map((e) => Serializable.deserialize<T>(e.data()))
                  .toList(),
        );
  }

  @override
  Stream<T> getById(String id) {
    return _collectionRef.doc(id).snapshots().map((event) => event.exists
        ? Serializable.deserialize<T>(event.data())
        : throw NotFoundError('No [$T] with id [$id] found'));
  }

  @override
  Stream<List<T>> getBy(List<Where> queries) {
    Query collectionQuery = _collectionRef;
    for (var query in queries) {
      if (query.isEqualTo) {
        collectionQuery = collectionQuery.where(
          query.fieldName,
          isEqualTo: query.fieldValue,
        );
      }
      if (query.isArrayContains) {
        collectionQuery = collectionQuery.where(
          query.fieldValue,
          arrayContains: query.fieldValue,
        );
      }
    }
    return collectionQuery.snapshots().map(
          (event) => event.docs.isEmpty
              ? []
              : event.docs
                  .map((e) => Serializable.deserialize<T>(e.data()))
                  .toList(),
        );
  }
}
