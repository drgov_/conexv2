import 'package:equatable/equatable.dart';
import 'package:uuid/uuid.dart';

import '../../auth/data/app_user.dart';
import '../../game/data/game.dart';
import '../../team/data/team.dart';

abstract class Serializable<T> extends Equatable {
  final String id;

  Serializable([String? id]) : id = id ?? const Uuid().v4();

  factory Serializable.fromMap(Map<String, dynamic> map) {
    switch (T) {
      case AppUser:
        return AppUser.fromMap(map) as Serializable<T>;
      case Game:
        return Game.fromMap(map) as Serializable<T>;
      case Team:
        return Team.fromMap(map) as Serializable<T>;
      default:
        throw UnimplementedError();
    }
  }

  Map<String, dynamic> toMap();

  static G deserialize<G extends Serializable>(Object? map) {
    return Serializable<G>.fromMap(map as Map<String, dynamic>) as G;
  }
}
