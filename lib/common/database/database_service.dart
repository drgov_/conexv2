class Where {
  final String fieldName;
  final String queryCondition;
  final dynamic fieldValue;

  Where({
    required this.fieldName,
    required this.queryCondition,
    required this.fieldValue,
  });

  bool get isEqualTo => queryCondition == 'equalsTo';
  bool get isArrayContains => queryCondition == 'arrayContains';
}

abstract class DatabaseService<T> {
  Future<String> add(T data);
  Stream<List<T>> getAll();
  Stream<T> getById(String id);
  Stream<List<T>> getBy(List<Where> queries);
  Future<void> update(String id, Map<String, Object?> data);
  Future<void> deleteById(String id);
  Future<void> deleteBy(List<Where> queries);
}
