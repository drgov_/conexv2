import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../common/database/_.dart';
import '../../team/provider/_.dart';
import '../data/_.dart';
import 'games_state_notifier.dart';

export 'games_state_notifier.dart';

final gameServiceProvider = Provider(
  (ref) => GameService(
    FStoreService(FirebaseFirestore.instance, 'games'),
    ref.watch(teamServiceProvider),
  ),
);

final gamesProvider = StreamProvider.autoDispose<List<Game>>(
  (ref) => ref.watch(gameServiceProvider).getGames(),
);

final _gamesControlProviders = StateNotifierProvider.family
    .autoDispose<GamesStateNotifier, GamesState, String>(
  (ref, id) => GamesStateNotifier(ref.watch(gameServiceProvider)),
);

final gamesHomeProvider = _gamesControlProviders('games-home');
final addGameProvider = _gamesControlProviders('add-game');
