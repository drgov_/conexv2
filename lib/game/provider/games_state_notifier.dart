import 'dart:async';

import 'package:conex/common/data/_.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../data/_.dart';

class GamesState extends AsyncState {
  const GamesState();
}

class GamesInit extends InitState implements GamesState {
  const GamesInit();
}

class GamesLoading extends LoadingState implements GamesState {
  const GamesLoading();
}

class GamesSuccess extends SuccessState implements GamesState {
  const GamesSuccess(AppSuccess success) : super(success);
}

class GamesError extends ErrorState implements GamesState {
  const GamesError(AppError error) : super(error);
}

class GamesStateNotifier extends StateNotifier<GamesState> {
  final GameService _gameService;

  GamesStateNotifier(this._gameService) : super(const GamesInit());

  Future<void> addGame(Game game) async {
    try {
      state = const GamesLoading();

      await _gameService.addGame(game);

      state = GamesSuccess(AppSuccess('${game.name} adaugat'));
    } on AppError catch (error) {
      state = GamesError(error);
    }
  }

  Future<void> deleteGame(Game game) async {
    try {
      state = const GamesLoading();

      await _gameService.deleteGameById(game.id);

      state = GamesSuccess(AppSuccess('${game.name} sters'));
    } on AppError catch (error) {
      state = GamesError(error);
    }
  }
}
