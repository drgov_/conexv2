import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/utils/_.dart';
import '../../../common/widgets/_.dart';
import 'add_game_form.dart';

class AddGameScreen extends ConsumerWidget {
  const AddGameScreen({Key? key}) : super(key: key);

  static Route route() => MaterialPageRoute(
        builder: (_) => const AddGameScreen(),
      );

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ScreenContainer(
      child: const AddGameForm(),
      floatingContent: FloatingNav(
        routes: [settingsRoute],
      ),
      centered: true,
    );
  }
}
