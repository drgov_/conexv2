import 'package:conex/common/data/_.dart';
import 'package:conex/common/sizes.dart';
import 'package:conex/common/utils/_.dart';
import 'package:conex/common/widgets/_.dart';
import 'package:conex/game/data/_.dart';
import 'package:conex/game/provider/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AddGameForm extends ConsumerStatefulWidget {
  const AddGameForm({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _AddGameFormState();
}

class _AddGameFormState extends ConsumerState<AddGameForm> {
  final nameController = TextEditingController();
  final maxTeamController = TextEditingController();

  @override
  void dispose() {
    disposeControllers([nameController, maxTeamController]);
    super.dispose();
  }

  Future<void> addGame() async {
    await ref.read(addGameProvider.notifier).addGame(Game(
        name: nameController.text.trim(),
        maxTeam: maxTeamController.text.trim()));
  }

  void onSuccess(AppSuccess success) {
    showSuccessSnackBar(context, success.message);
    clearControllers([nameController, maxTeamController]);
  }

  @override
  Widget build(BuildContext context) {
    ref.listen<GamesState>(addGameProvider, (prev, next) {
      next.effect(
          onError: (error) => showErrorSnackBar(context, error.message),
          onSuccess: onSuccess);
    });

    final addGameState = ref.watch(addGameProvider);

    return addGameState.returnOn(
      onLoading: () => const Loading(),
      onElse: () => AppColumn(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CstmTextField(
            controller: nameController,
            hintText: 'Nume joc',
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
          NumberTextField(
            controller: maxTeamController,
            hintText: 'Numar maxim jucatori in echipa',
            margin: const EdgeInsets.only(bottom: Spacing.l),
          ),
          Btn(
            onPressed: addGame,
            child: const Text('Adauga joc'),
          )
        ],
      ),
    );
  }
}
