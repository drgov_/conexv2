import 'package:conex/auth/provider/_.dart';
import 'package:conex/common/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/widgets/_.dart';
import '../../data/game.dart';
import '../../provider/_.dart';

class GameCard extends ConsumerWidget {
  final Game game;

  const GameCard(this.game, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final loggedUser = ref.watch(loggedUserProvider).value!;

    return AppCard(
      margin: const EdgeInsets.only(bottom: Spacing.s),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AccentHeader(
                  game.name,
                  bold: true,
                  margin: const EdgeInsets.only(bottom: Spacing.m),
                ),
                PlayersDisplay(
                    int.parse(game.maxTeam), int.parse(game.maxTeam)),
              ],
            ),
          ),
          if (loggedUser.isAdmin)
            IconButton(
              onPressed: () =>
                  ref.read(gamesHomeProvider.notifier).deleteGame(game),
              icon: const Icon(Icons.delete),
            ),
        ],
      ),
    );
  }
}
