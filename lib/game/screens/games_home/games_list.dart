import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/widgets/_.dart';
import '../../data/game.dart';
import '../../provider/_.dart';
import 'game_card.dart';

class GamesList extends ConsumerWidget {
  const GamesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final gamesState = ref.watch(gamesProvider);

    return gamesState.maybeWhen(
      data: (data) => AppList<Game>(
        whenEmpty: Header('Niciun joc'),
        data: data,
        item: (Game game) => GameCard(game),
      ),
      orElse: () => const Loading(),
    );
  }
}
