import 'package:conex/common/utils/_.dart';
import 'package:conex/game/provider/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/widgets/_.dart';
import 'games_list.dart';

class GamesHomeScreen extends ConsumerWidget {
  const GamesHomeScreen({Key? key}) : super(key: key);

  static Route route() => MaterialPageRoute<void>(
        builder: (_) => const GamesHomeScreen(),
      );

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<GamesState>(gamesHomeProvider, (previous, next) {
      next.effect(
          onSuccess: (success) =>
              showSuccessSnackBar(context, success.message));
    });

    return ScreenContainer(
      child: const Center(
        child: GamesList(),
      ),
      floatingContent: FloatingNav(
        routes: [settingsRoute, addGameRoute],
      ),
    );
  }
}
