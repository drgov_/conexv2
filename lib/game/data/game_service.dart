import '../../common/data/app_error.dart';
import '../../common/data/validator.dart';
import '../../common/database/database_service.dart';
import '../../team/data/team_service.dart';
import 'game.dart';

class GameService {
  final DatabaseService<Game> _databaseService;
  final TeamService _teamService;

  GameService(this._databaseService, this._teamService);

  Future<String> addGame(Game game) async {
    Validator.validateGameData(game);
    if (await getGameByName(game.name).first != null) {
      throw ConflictError('${game.name} exista deja');
    }
    await _databaseService.add(game);
    return game.id;
  }

  Future<void> deleteGameById(String id) async {
    await _teamService.deleteTeamsByGameId(id);
    await _databaseService.deleteById(id);
  }

  Stream<List<Game>> getGames() {
    return _databaseService.getAll();
  }

  Stream<Game> getGameById(String id) {
    return _databaseService.getById(id);
  }

  Stream<Game?> getGameByName(String name) {
    return _databaseService.getBy([
      Where(fieldName: 'name', queryCondition: 'equalsTo', fieldValue: name)
    ]).map((event) => event.isEmpty ? null : event[0]);
  }
}
