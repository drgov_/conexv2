import '../../common/database/serializable.dart';

class Game extends Serializable<Game> {
  final String name;
  final String maxTeam;

  Game({
    String? id,
    required this.name,
    required this.maxTeam,
  }) : super(id);

  @override
  List<Object?> get props => [name, maxTeam];

  factory Game.fromMap(Map<String, dynamic> map) => Game(
        id: map['id'],
        name: map['name'],
        maxTeam: map['maxTeam'].toString(),
      );

  @override
  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "maxTeam": int.parse(maxTeam),
      };
}
