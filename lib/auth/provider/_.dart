import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../common/database/_.dart';
import '../data/_.dart';
import 'auth_state_notifier.dart';

export 'auth_state_notifier.dart';

final appUserServiceProvider = Provider(
  (ref) => AppUserService(
    FStoreService(FirebaseFirestore.instance, 'users'),
  ),
);

final authServiceProvider = Provider(
  (ref) => FBaseAuthService(
    ref.watch(appUserServiceProvider),
  ),
);

final loggedUserProvider = StreamProvider<AppUser?>(
  (ref) => ref.watch(authServiceProvider).getLoggedUser(),
);

final _authControlProviders = StateNotifierProvider.family
    .autoDispose<AuthStateNotifier, AuthState, String>(
  (ref, id) => AuthStateNotifier(ref.watch(authServiceProvider)),
);
final authProvider = _authControlProviders('');

final loginProvider = _authControlProviders('login');
final registerProvider = _authControlProviders('register');
