import 'dart:async';

import 'package:conex/common/data/_.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../data/_.dart';

class AuthState extends AsyncState {
  const AuthState();
}

class AuthInit extends AuthState {
  const AuthInit();
}

class AuthLoading extends LoadingState implements AuthState {
  const AuthLoading();
}

class AuthSuccess extends SuccessState implements AuthState {
  const AuthSuccess();
}

class AuthError extends ErrorState implements AuthState {
  const AuthError(AppError error) : super(error);
}

class AuthStateNotifier extends StateNotifier<AuthState> {
  final AuthService _authService;

  AuthStateNotifier(this._authService) : super(const AuthInit());

  Future<void> register({
    required String email,
    required String username,
    required String password,
  }) async {
    try {
      state = const AuthLoading();

      await _authService.register(email, username, password);

      state = const AuthSuccess();
    } on AppError catch (error) {
      state = AuthError(error);
    }
  }

  Future<void> logIn({
    required String email,
    required String password,
  }) async {
    try {
      state = const AuthLoading();

      await _authService.logInWithEmailPassword(email, password);

      state = const AuthSuccess();
    } on AppError catch (error) {
      state = AuthError(error);
    }
  }

  Future<void> logOut() async {
    state = const AuthLoading();

    await _authService.logOut();

    state = const AuthSuccess();
  }
}
