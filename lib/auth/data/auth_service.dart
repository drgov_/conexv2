import 'package:conex/common/data/_.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';

import 'app_user.dart';
import 'app_user_service.dart';

abstract class AuthService {
  Stream<AppUser?> getLoggedUser();
  Future<void> logInWithEmailPassword(String email, String password);
  Future<void> logOut();
  Future<String> register(String email, String username, String password);
}

class FBaseAuthService extends AuthService {
  final FirebaseAuth _firebaseAuth;
  final AppUserService _appUserService;

  FBaseAuthService(this._appUserService)
      : _firebaseAuth = FirebaseAuth.instance;

  @override
  Stream<AppUser?> getLoggedUser() {
    return _firebaseAuth
        .authStateChanges()
        .switchMap(_appUserService.getAppUserFromFBaseUser);
  }

  @override
  Future<void> logInWithEmailPassword(String email, String password) async {
    try {
      Validator.validateLoginData(email, password);
      await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'invalid-email':
          throw InvalidError('Email invalid');
        case 'user-not-found':
        case 'wrong-password':
          throw NotFoundError('Nu exista user cu aceste date');
        default:
          rethrow;
      }
    }
  }

  @override
  Future<void> logOut() async {
    await _firebaseAuth.signOut();
  }

  @override
  Future<String> register(
    String email,
    String username,
    String password,
  ) async {
    try {
      Validator.validateRegisterData(email, username, password);
      final userCred = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      final id = await _appUserService.addUser(
        AppUser(
          id: userCred.user!.uid,
          email: email,
          username: username,
        ),
      );
      return id;
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'email-already-in-use':
          throw ConflictError('Un user cu acest email exista deja');
        case 'invalid-email':
          throw InvalidError('Email invalid');
        case 'weak-password':
          throw OtherError('Parola slaba (min 6 caractere)');
        default:
          rethrow;
      }
    }
  }
}
