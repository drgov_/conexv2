import 'package:conex/team/data/team.dart';

import '../../common/database/serializable.dart';

class AppUser extends Serializable<AppUser> {
  final String email;
  final String username;
  final bool isAdmin;

  AppUser({
    String? id,
    required this.email,
    required this.username,
    this.isAdmin = false,
  }) : super(id);

  @override
  List<Object?> get props => [email, username, isAdmin];

  factory AppUser.fromMap(Map<String, dynamic> map) => AppUser(
        id: map['id'],
        email: map['email'],
        username: map['username'],
        isAdmin: map['isAdmin'],
      );

  @override
  Map<String, dynamic> toMap() => {
        'id': id,
        'email': email,
        'username': username,
        'isAdmin': isAdmin,
      };

  bool ownsTeam(Team team) => team.userId == id;
  bool isInTeam(Team team) => team.usersIds.any((element) => element == id);
}
