import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';

import '../../common/database/database_service.dart';
import 'app_user.dart';

class AppUserService {
  final DatabaseService<AppUser> _databaseService;

  AppUserService(this._databaseService);

  Future<String> addUser(AppUser user) async {
    await _databaseService.add(user);
    return user.id;
  }

  Future<void> deleteUserById(String id) async {
    await _databaseService.deleteById(id);
  }

  Stream<List<AppUser>> getUsers() {
    return _databaseService.getAll();
  }

  Stream<AppUser> getUserById(String id) {
    return _databaseService.getById(id);
  }

  Stream<AppUser?> getAppUserFromFBaseUser(User? user) async* {
    if (user == null) {
      yield null;
    } else {
      yield* getUserById(user.uid);
    }
  }

  Stream<List<AppUser>> getUsersByIds(List<String> ids) {
    return CombineLatestStream.list(ids.map((id) => getUserById(id)))
        .defaultIfEmpty([]);
  }
}
