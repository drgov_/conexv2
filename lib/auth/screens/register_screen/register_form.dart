import 'package:conex/auth/provider/_.dart';
import 'package:conex/common/sizes.dart';
import 'package:conex/common/utils/_.dart';
import 'package:conex/common/widgets/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RegisterForm extends ConsumerStatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _RegisterFormState();
}

class _RegisterFormState extends ConsumerState<RegisterForm> {
  final emailController = TextEditingController();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    disposeControllers(
        [emailController, usernameController, passwordController]);
    super.dispose();
  }

  Future<void> register() async {
    await ref.read(registerProvider.notifier).register(
        email: emailController.text.trim(),
        username: usernameController.text.trim(),
        password: passwordController.text.trim());
  }

  @override
  Widget build(BuildContext context) {
    final registerState = ref.watch(registerProvider);

    return registerState.returnOn(
      onLoading: () => const Loading(),
      onElse: () => Column(
        children: [
          CstmTextField(
            controller: emailController,
            hintText: 'Email',
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
          CstmTextField(
            controller: usernameController,
            hintText: 'Username',
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
          HiddenField(
            controller: passwordController,
            hintText: 'Parola',
            margin: const EdgeInsets.only(bottom: Spacing.l),
          ),
          Btn(
            onPressed: register,
            child: const Text('Inregistrare'),
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
        ],
      ),
    );
  }
}
