import 'package:conex/auth/provider/_.dart';
import 'package:conex/auth/screens/register_screen/register_form.dart';
import 'package:conex/common/utils/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/widgets/_.dart';

class RegisterScreen extends ConsumerWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  static Route route() => MaterialPageRoute(
        builder: (_) => const RegisterScreen(),
      );

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<AuthState>(registerProvider, (previous, next) {
      next.effect(
          onError: (error) => showErrorSnackBar(context, error.message),
          onSuccess: (success) => popPage(context));
    });

    return ScreenContainer(
      child: const RegisterForm(),
      centered: true,
      floatingContent: FloatingNav(
        routes: [loginRoute],
      ),
    );
  }
}
