import 'package:conex/auth/provider/_.dart';
import 'package:conex/common/utils/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/widgets/_.dart';
import 'login_form.dart';

class LoginScreen extends ConsumerWidget {
  const LoginScreen({Key? key}) : super(key: key);

  static Route route() => MaterialPageRoute(
        builder: (_) => const LoginScreen(),
      );

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<AuthState>(loginProvider, (prev, next) {
      next.effect(
          onError: (error) => showErrorSnackBar(context, error.message));
    });

    return ScreenContainer(
      child: const LoginForm(),
      centered: true,
      floatingContent: FloatingNav(
        routes: [registerRoute],
      ),
    );
  }
}
