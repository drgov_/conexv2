import 'package:conex/auth/provider/_.dart';
import 'package:conex/common/sizes.dart';
import 'package:conex/common/utils/_.dart';
import 'package:conex/common/widgets/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class LoginForm extends ConsumerStatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LoginFormState();
}

class _LoginFormState extends ConsumerState<LoginForm> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    disposeControllers([emailController, passwordController]);
    super.dispose();
  }

  Future<void> login() async {
    await ref.read(loginProvider.notifier).logIn(
        email: emailController.text.trim(),
        password: passwordController.text.trim());
  }

  @override
  Widget build(BuildContext context) {
    final loginState = ref.watch(loginProvider);

    return loginState.returnOn(
      onLoading: () => const Loading(),
      onElse: () => Column(
        children: [
          CstmTextField(
            controller: emailController,
            hintText: 'Email',
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
          HiddenField(
            controller: passwordController,
            hintText: 'Parola',
            margin: const EdgeInsets.only(bottom: Spacing.l),
          ),
          Btn(
            onPressed: login,
            child: const Text('Intra in cont'),
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
        ],
      ),
    );
  }
}
