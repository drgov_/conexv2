import 'dart:async';

import 'package:conex/auth/data/app_user.dart';
import 'package:conex/common/data/_.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../data/_.dart';

class TeamsState extends AsyncState {
  const TeamsState();
}

class TeamsInit extends InitState implements TeamsState {
  const TeamsInit();
}

class TeamsLoading extends LoadingState implements TeamsState {
  const TeamsLoading();
}

class TeamsSuccess extends SuccessState implements TeamsState {
  const TeamsSuccess(AppSuccess success) : super(success);
}

class TeamsError extends ErrorState implements TeamsState {
  const TeamsError(AppError error) : super(error);
}

class TeamsStateNotifier extends StateNotifier<TeamsState> {
  final TeamService _teamService;

  TeamsStateNotifier(this._teamService) : super(const TeamsInit());

  Future<void> addTeam(Team team) async {
    try {
      state = const TeamsLoading();

      await _teamService.addTeam(team);

      state = TeamsSuccess(AppSuccess('Echipa ${team.name} cauta jucatori'));
    } on AppError catch (error) {
      state = TeamsError(error);
    }
  }

  Future<void> deleteTeam(Team team) async {
    try {
      state = const TeamsLoading();

      await _teamService.deleteTeamById(team.id);

      state = TeamsSuccess(AppSuccess('Echipa ${team.name} stearsa'));
    } on AppError catch (error) {
      state = TeamsError(error);
    }
  }

  Future<void> addUserToTeam(AppUser user, Team team) async {
    try {
      state = const TeamsLoading();

      await _teamService.updateTeam(
          team.id, team.copyWith(usersIds: [...team.usersIds, user.id]));

      state = TeamsSuccess(AppSuccess('Te-ai alaturat echipei ${team.name}'));
    } on AppError catch (error) {
      state = TeamsError(error);
    }
  }

  Future<void> removeUserFromTeam(AppUser user, Team team) async {
    try {
      state = const TeamsLoading();

      await _teamService.updateTeam(
          team.id,
          team.copyWith(
              usersIds: team.usersIds
                ..removeWhere((element) => element == user.id)));

      state = TeamsSuccess(AppSuccess('Ai iesi din echipa ${team.name}'));
    } on AppError catch (error) {
      state = TeamsError(error);
    }
  }
}
