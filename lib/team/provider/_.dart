import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:conex/game/data/_.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../auth/provider/_.dart';
import '../../common/database/_.dart';
import '../../game/provider/_.dart';
import '../data/_.dart';
import 'teams_state_notifier.dart';

final teamServiceProvider = Provider(
  (ref) => TeamService(
    FStoreService(FirebaseFirestore.instance, 'teams'),
  ),
);

final teamGameUsersServiceProvider = Provider(
  (ref) => TeamGameUsersService(
    ref.watch(teamServiceProvider),
    ref.watch(gameServiceProvider),
    ref.watch(appUserServiceProvider),
  ),
);

final teamsGamesUsersProvider = StreamProvider.autoDispose(
  (ref) => ref.watch(teamGameUsersServiceProvider).getTeamsGamesUsers(),
);

final teamsGamesUsersByGameProvider = StreamProvider.family.autoDispose(
  (ref, Game game) =>
      ref.watch(teamGameUsersServiceProvider).getTeamsGamesUsersByGame(game),
);

final teamGameUsersProvider = StreamProvider.family.autoDispose(
  (ref, String teamId) =>
      ref.watch(teamGameUsersServiceProvider).getTeamGameUsersByTeamId(teamId),
);

final _teamsControlProviders = StateNotifierProvider.family
    .autoDispose<TeamsStateNotifier, TeamsState, String>(
  (ref, id) => TeamsStateNotifier(ref.watch(teamServiceProvider)),
);

final teamsHomeProvider = _teamsControlProviders('teams-home');
final teamDetailsProvider = _teamsControlProviders('team-details');
final addTeamProvider = _teamsControlProviders('add-team');
