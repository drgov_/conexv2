import '../../auth/data/app_user.dart';
import '../../game/data/game.dart';
import 'team.dart';

class TeamGameUsers {
  final Team team;
  final Game game;
  final List<AppUser> users;

  TeamGameUsers({
    required this.team,
    required this.game,
    required this.users,
  });

  TeamGameUsers copyWith({
    Team? team,
    Game? game,
    List<AppUser>? users,
  }) =>
      TeamGameUsers(
        team: team ?? this.team,
        game: game ?? this.game,
        users: users ?? this.users,
      );
}
