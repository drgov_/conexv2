import 'package:rxdart/rxdart.dart';

import '../../auth/data/_.dart';
import '../../game/data/_.dart';
import 'team.dart';
import 'team_game_users.dart';
import 'team_service.dart';

class TeamGameUsersService {
  final TeamService _teamService;
  final GameService _gameService;
  final AppUserService _appUserService;

  TeamGameUsersService(
    this._teamService,
    this._gameService,
    this._appUserService,
  );

  Stream<TeamGameUsers> getTeamGameUsersByTeamId(String id) {
    final teamStream = _teamService.getTeamById(id);
    final gameStream =
        teamStream.flatMap((value) => _gameService.getGameById(value.gameId));
    final usersStream = teamStream
        .flatMap((value) => _appUserService.getUsersByIds(value.usersIds));
    return Rx.combineLatest3(
      teamStream,
      gameStream,
      usersStream,
      (Team team, Game game, List<AppUser> users) =>
          TeamGameUsers(team: team, game: game, users: users),
    ).handleError((e) {});
  }

  Stream<List<TeamGameUsers>> getTeamsGamesUsers() {
    return _teamService.getTeams().flatMap(
          (teams) => CombineLatestStream.list(
                  teams.map((e) => getTeamGameUsersByTeamId(e.id)))
              .defaultIfEmpty([]),
        );
  }

  Stream<List<TeamGameUsers>> getTeamsGamesUsersByGame(Game game) {
    return _teamService.getTeamsByGame(game).flatMap(
          (teams) => CombineLatestStream.list(
                  teams.map((e) => getTeamGameUsersByTeamId(e.id)))
              .defaultIfEmpty([]),
        );
  }
}
