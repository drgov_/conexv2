import '../../common/database/serializable.dart';

class Team extends Serializable<Team> {
  final String name;
  final String description;
  final String neededPlayers;
  final String severity;
  final String gameId;
  final String userId;
  final List<String> usersIds;

  Team({
    String? id,
    required this.name,
    this.description = '',
    required this.neededPlayers,
    this.severity = 'low',
    required this.gameId,
    required this.userId,
    List<String>? usersIds,
  })  : usersIds = usersIds ?? [userId],
        super(id);

  @override
  List<Object?> get props =>
      [name, description, neededPlayers, severity, gameId, userId];

  factory Team.fromMap(Map<String, dynamic> map) => Team(
        id: map['id'],
        name: map['name'],
        description: map['description'],
        neededPlayers: map['neededPlayers'].toString(),
        severity: map['severity'],
        gameId: map['gameId'],
        userId: map['userId'],
        usersIds: List<String>.from(map['usersIds']),
      );

  @override
  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'description': description,
        'neededPlayers': int.parse(neededPlayers),
        'severity': severity,
        'gameId': gameId,
        'userId': userId,
        'usersIds': usersIds,
      };

  int get currentPlayersN => usersIds.length;
  int get neededPlayersN => int.parse(neededPlayers);
  bool get isFull => currentPlayersN == neededPlayersN;

  Team copyWith({
    String? name,
    String? description,
    String? neededPlayers,
    String? severity,
    String? gameId,
    String? userId,
    List<String>? usersIds,
  }) =>
      Team(
        id: id,
        name: name ?? this.name,
        description: description ?? this.description,
        neededPlayers: neededPlayers ?? this.neededPlayers,
        severity: severity ?? this.severity,
        gameId: gameId ?? this.gameId,
        userId: userId ?? this.userId,
        usersIds: usersIds ?? this.usersIds,
      );
}
