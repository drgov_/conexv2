import 'package:conex/common/data/validator.dart';
import 'package:conex/game/data/_.dart';

import '../../common/database/database_service.dart';
import 'team.dart';

class TeamService {
  final DatabaseService<Team> _databaseService;

  TeamService(this._databaseService);

  Future<String> addTeam(Team team) async {
    Validator.validateTeamData(team);
    await _databaseService.add(team);
    return team.id;
  }

  Future<void> deleteTeamById(String id) async {
    await _databaseService.deleteById(id);
  }

  Future<void> deleteTeamsByGameId(String gameId) async {
    await _databaseService.deleteBy([
      Where(fieldName: 'gameId', queryCondition: 'equalsTo', fieldValue: gameId)
    ]);
  }

  Future<String> updateTeam(String id, Team newTeam) async {
    await _databaseService.update(id, newTeam.toMap());
    return id;
  }

  Stream<List<Team>> getTeams() {
    return _databaseService.getAll();
  }

  Stream<List<Team>> getTeamsByGame(Game game) {
    return _databaseService.getBy([
      Where(
          fieldName: 'gameId', queryCondition: 'equalsTo', fieldValue: game.id)
    ]);
  }

  Stream<Team> getTeamById(String id) {
    return _databaseService.getById(id);
  }
}
