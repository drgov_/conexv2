import 'package:flutter/material.dart';

import '../../../common/utils/_.dart';
import '../../../common/widgets/_.dart';
import 'add_team_form.dart';

class AddTeamScreen extends StatelessWidget {
  const AddTeamScreen({Key? key}) : super(key: key);

  static Route route() => MaterialPageRoute(
        builder: (_) => const AddTeamScreen(),
      );

  @override
  Widget build(BuildContext context) {
    return ScreenContainer(
      child: const AddTeamForm(),
      centered: true,
      floatingContent: FloatingNav(
        routes: [settingsRoute],
      ),
    );
  }
}
