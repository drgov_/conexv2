import 'dart:async';

import 'package:conex/auth/provider/_.dart';
import 'package:conex/common/data/_.dart';
import 'package:conex/common/sizes.dart';
import 'package:conex/common/utils/_.dart';
import 'package:conex/common/widgets/_.dart';
import 'package:conex/game/data/_.dart';
import 'package:conex/game/provider/_.dart';
import 'package:conex/team/data/_.dart';
import 'package:conex/team/provider/_.dart';
import 'package:conex/team/provider/teams_state_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:sensors_plus/sensors_plus.dart';

class _FormState {
  final Game? game;
  final bool showTip;
  final String severity;

  const _FormState({
    this.game,
    this.showTip = false,
    this.severity = 'low',
  });

  _FormState reset() => const _FormState();
  _FormState setGame(Game? game) =>
      _FormState(game: game, showTip: showTip, severity: severity);
  _FormState setSeverity(String severity) =>
      _FormState(game: game, showTip: showTip, severity: severity);
  _FormState setShowTip(bool showTip) =>
      _FormState(game: game, showTip: showTip, severity: severity);
  _FormState showTipResetSeverity() => setShowTip(true).setSeverity('low');
  _FormState hideTip() => setShowTip(false);
}

final _formStateProvider =
    StateProvider.autoDispose<_FormState>((ref) => const _FormState());

const _SHAKE_THRESHOLD = 2.5; //22.5
const _MIN_TIME_BETWEEN_SHAKES_MS = 300;
const _LOWER_LIMIT = 6;
const _UPPER_LIMIT = 12;
const _LISTEN_TIME = 4;

class AddTeamForm extends ConsumerStatefulWidget {
  const AddTeamForm({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _AddTeamFormState();
}

class _AddTeamFormState extends ConsumerState<AddTeamForm> {
  final nameController = TextEditingController();
  final neededPlayersController = TextEditingController();
  final descriptionController = TextEditingController();
  StreamSubscription? accelerometerSub;

  @override
  void dispose() {
    disposeControllers([
      nameController,
      neededPlayersController,
      descriptionController,
    ]);
    super.dispose();
  }

  void resetFields() {
    clearControllers(
        [nameController, neededPlayersController, descriptionController]);
    ref.read(_formStateProvider.notifier).update((state) => state.reset());
  }

  Future<void> shake() async {
    final notifier = ref.read(_formStateProvider.notifier);
    notifier.update((state) => state.showTipResetSeverity());
    var lastShake = 0;
    var shakesCount = 0;
    accelerometerSub = userAccelerometerEvents.listen((event) {
      final now = DateTime.now().millisecondsSinceEpoch;
      if (now - lastShake > _MIN_TIME_BETWEEN_SHAKES_MS) {
        if (getAccel(event) > _SHAKE_THRESHOLD) {
          lastShake = now;
          shakesCount++;
        }
        if (_LOWER_LIMIT < shakesCount && shakesCount < _UPPER_LIMIT) {
          notifier.update((state) => state.setSeverity('medium'));
        }
        if (shakesCount >= _UPPER_LIMIT) {
          notifier.update((state) => state.setSeverity('high'));
        }
      }
    });
    await Future.delayed(const Duration(seconds: _LISTEN_TIME), () {
      notifier.update((state) => state.hideTip());
      accelerometerSub!.cancel();
      print(shakesCount);
    });
  }

  Future<void> createTeam() async {
    final loggedUser = ref.watch(loggedUserProvider).value!;
    final addTeamFormState = ref.watch(_formStateProvider);
    final team = Team(
      name: nameController.text.trim(),
      neededPlayers: neededPlayersController.text.trim(),
      description: descriptionController.text.trim(),
      severity: addTeamFormState.severity,
      gameId: addTeamFormState.game?.id ?? '',
      userId: loggedUser.id,
    );
    await ref.read(addTeamProvider.notifier).addTeam(team);
  }

  void onSuccess(AppSuccess success) {
    showSuccessSnackBar(context, success.message);
    resetFields();
    popPage(context);
  }

  @override
  Widget build(BuildContext context) {
    ref.listen<TeamsState>(addTeamProvider, (previous, next) {
      next.effect(
          onError: (error) => showErrorSnackBar(context, error.message),
          onSuccess: onSuccess);
    });
    final notifier = ref.read(_formStateProvider.notifier);
    final addTeamState = ref.watch(addTeamProvider);
    final games = ref
        .watch(gamesProvider)
        .maybeWhen(data: (data) => data, orElse: () => <Game>[]);
    final formState = ref.watch(_formStateProvider);

    return addTeamState.returnOn(
      onLoading: () => const Loading(),
      onElse: () => AppColumn(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CstmTextField(
            controller: nameController,
            hintText: 'Nume echipa',
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
          CstmDropDownField<Game>(
            value: formState.game,
            item: (game) => CstmText(game.name),
            items: games,
            onChanged: (value) =>
                notifier.update((state) => state.setGame(value)),
            hintText: 'Joc',
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
          NumberTextField(
            controller: neededPlayersController,
            hintText: 'Numar jucatori',
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
          CstmTextField(
            controller: descriptionController,
            hintText: 'Descriere',
            margin: const EdgeInsets.only(bottom: Spacing.l),
          ),
          Btn(
            onPressed: shake,
            child: const Text('Seteaza disperarea'),
            margin: const EdgeInsets.only(bottom: Spacing.s),
          ),
          if (formState.showTip) _ShakeTip(),
          Padding(
            padding: const EdgeInsets.only(bottom: Spacing.m),
            child: SeverityDisplay(formState.severity),
          ),
          Btn(
            onPressed: createTeam,
            child: const Text('Cauta jucatori'),
          )
        ],
      ),
    );
  }
}

class _ShakeTip extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppColumn(
      crossAxisAlignment: CrossAxisAlignment.center,
      margin: const EdgeInsets.only(bottom: Spacing.s),
      children: [
        Image.asset(
          'assets/imgs/shake.png',
          width: 72,
        ),
        const CstmText('Scutura telefonul pentru a dovedi'),
      ],
    );
  }
}
