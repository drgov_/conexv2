import 'package:conex/team/screens/teams_home_screen/teams_filter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/utils/navigation_utils.dart';
import '../../../common/widgets/_.dart';
import 'teams_list.dart';

class TeamsHomeScreen extends ConsumerStatefulWidget {
  const TeamsHomeScreen({Key? key}) : super(key: key);

  static Route route() => MaterialPageRoute(
        builder: (_) => const TeamsHomeScreen(),
      );

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _TeamsScreenState();
}

class _TeamsScreenState extends ConsumerState<TeamsHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return ScreenContainer(
      child: Column(
        children: const [
          TeamsFilter(),
          TeamsList(),
        ],
      ),
      floatingContent: FloatingNav(
        routes: [settingsRoute, gamesHomeRoute, addTeamRoute],
      ),
    );
  }
}
