import 'package:conex/team/screens/teams_home_screen/teams_filter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/widgets/_.dart';
import '../../data/team_game_users.dart';
import '../../provider/_.dart';
import 'team_card.dart';

class TeamsList extends ConsumerWidget {
  const TeamsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final teamsFilter = ref.watch(teamsFilterProvider);
    final gamesState = teamsFilter.filterByGame
        ? ref.watch(teamsGamesUsersByGameProvider(teamsFilter.game!))
        : ref.watch(teamsGamesUsersProvider);

    return gamesState.maybeWhen(
      data: (data) => AppList<TeamGameUsers>(
        whenEmpty: Header('Nicio echipa'),
        data: data,
        item: (TeamGameUsers teamGameUsers) => TeamCard(teamGameUsers),
      ),
      orElse: () => const Loading(),
    );
  }
}
