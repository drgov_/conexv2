import 'package:conex/common/sizes.dart';
import 'package:conex/common/utils/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/widgets/_.dart';
import '../../data/team_game_users.dart';

class TeamCard extends ConsumerWidget {
  final TeamGameUsers _teamGameUsers;

  const TeamCard(this._teamGameUsers, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return AppCard(
      margin: const EdgeInsets.only(bottom: Spacing.s),
      onTap: () => teamDetailsRoute(_teamGameUsers.team.id).goTo(context),
      child: AppColumn(
        children: [
          AccentHeader(
            _teamGameUsers.team.name,
            fontSize: CstmFontSize.mh,
            bold: true,
            margin: const EdgeInsets.only(bottom: Spacing.s),
          ),
          Header(
            _teamGameUsers.game.name,
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
          AppRow(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              PlayersDisplay(
                _teamGameUsers.team.currentPlayersN,
                _teamGameUsers.team.neededPlayersN,
              ),
              SeverityDisplay(_teamGameUsers.team.severity),
            ],
          )
        ],
      ),
    );
  }
}
