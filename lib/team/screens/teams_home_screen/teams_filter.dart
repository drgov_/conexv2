import 'package:conex/common/sizes.dart';
import 'package:conex/common/widgets/_.dart';
import 'package:conex/game/data/game.dart';
import 'package:conex/game/provider/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class TeamsFilterState {
  final Game? game;

  const TeamsFilterState({this.game});

  bool get filterByGame => game != null;

  TeamsFilterState setGame(Game? game) => TeamsFilterState(game: game);
  TeamsFilterState resetGame() => const TeamsFilterState();
}

final teamsFilterProvider = StateProvider<TeamsFilterState>(
  (ref) => const TeamsFilterState(),
);

class TeamsFilter extends ConsumerWidget {
  const TeamsFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final teamsFilterNotifier = ref.read(teamsFilterProvider.notifier);
    final teamsFilter = ref.watch(teamsFilterProvider);
    final games = ref
        .watch(gamesProvider)
        .maybeWhen(data: (data) => data, orElse: () => <Game>[]);

    return AppRow(
      margin: const EdgeInsets.only(bottom: Spacing.m),
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: CstmDropDownField<Game>(
            value: teamsFilter.game,
            onChanged: (value) =>
                teamsFilterNotifier.update((state) => state.setGame(value)),
            items: games,
            item: (game) => CstmText(game.name),
            hintText: 'Joc',
          ),
        ),
        InkWell(
          borderRadius: BorderRadius.circular(25),
          onTap: () => teamsFilterNotifier.update((state) => state.resetGame()),
          child: const Icon(
            Icons.refresh,
            size: 36,
          ),
        )
      ],
    );
  }
}
