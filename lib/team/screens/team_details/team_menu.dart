import 'package:conex/auth/provider/_.dart';
import 'package:conex/common/utils/_.dart';
import 'package:conex/common/widgets/_.dart';
import 'package:conex/team/data/_.dart';
import 'package:conex/team/provider/_.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class TeamMenu extends ConsumerWidget {
  final TeamGameUsers data;

  const TeamMenu(this.data, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final loggedUser = ref.watch(loggedUserProvider).value!;
    final team = data.team;

    if (loggedUser.ownsTeam(team)) {
      Future<void> deleteTeam() async {
        await ref.read(teamDetailsProvider.notifier).deleteTeam(team);
        popPage(context);
      }

      return FloatingActionButton(
        onPressed: deleteTeam,
        child: const SvgIcon('delete_trash_can'),
      );
    }
    if (loggedUser.isInTeam(team)) {
      return FloatingActionButton(
        onPressed: () => ref
            .read(teamDetailsProvider.notifier)
            .removeUserFromTeam(loggedUser, team),
        child: const SvgIcon('exit'),
      );
    }
    if (!loggedUser.isInTeam(team) && !team.isFull) {
      return FloatingActionButton(
        onPressed: () => ref
            .read(teamDetailsProvider.notifier)
            .addUserToTeam(loggedUser, team),
        child: const SvgIcon('enter'),
      );
    }
    return Container();
  }
}
