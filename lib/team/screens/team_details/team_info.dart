import 'package:conex/auth/data/app_user.dart';
import 'package:conex/common/sizes.dart';
import 'package:conex/common/widgets/_.dart';
import 'package:conex/team/data/team_game_users.dart';
import 'package:conex/team/screens/team_details/team_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class TeamInfo extends ConsumerWidget {
  final TeamGameUsers _data;

  const TeamInfo(this._data, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ScreenContainer(
      child: AppColumn(
        children: [
          AccentHeader(
            _data.team.name,
            fontSize: CstmFontSize.lh,
            bold: true,
            margin: const EdgeInsets.only(bottom: Spacing.s),
          ),
          Header(
            'Joc: ${_data.game.name}',
            margin: const EdgeInsets.only(bottom: Spacing.m),
            fontSize: CstmFontSize.mh,
          ),
          CstmText(
            _data.team.description,
            margin: const EdgeInsets.only(bottom: Spacing.m),
          ),
          Header(
            'Jucatori (${_data.team.currentPlayersN}/${_data.team.neededPlayersN}):',
            margin: const EdgeInsets.only(bottom: Spacing.s),
          ),
          PlayersDisplay(
            _data.team.currentPlayersN,
            _data.team.neededPlayersN,
            margin: const EdgeInsets.only(bottom: Spacing.m),
            width: 32,
            height: 32,
          ),
          AppList<AppUser>(
            data: _data.users,
            item: (user) => AppCard(
              margin: const EdgeInsets.only(bottom: Spacing.s),
              child: Center(
                child: Header(
                  user.username,
                  fontSize: CstmFontSize.xsh,
                ),
              ),
            ),
          ),
        ],
      ),
      floatingContent: TeamMenu(_data),
    );
  }
}
