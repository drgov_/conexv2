import 'package:conex/common/utils/ui_utils.dart';
import 'package:conex/common/widgets/_.dart';
import 'package:conex/team/provider/_.dart';
import 'package:conex/team/provider/teams_state_notifier.dart';
import 'package:conex/team/screens/team_details/team_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class TeamDetailsScreen extends ConsumerWidget {
  final String teamId;

  const TeamDetailsScreen(this.teamId, {Key? key}) : super(key: key);

  static Route route(String teamId) => MaterialPageRoute(
        builder: (_) => TeamDetailsScreen(teamId),
      );

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<TeamsState>(teamDetailsProvider, (previous, next) {
      next.effect(
          onSuccess: (success) =>
              showSuccessSnackBar(context, success.message));
    });
    final teamGamUsersState = ref.watch(teamGameUsersProvider(teamId));

    return teamGamUsersState.maybeWhen(
      data: (data) => TeamInfo(data),
      orElse: () => const Loading(),
    );
  }
}
