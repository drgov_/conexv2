<style>
  .container {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .container img {
    width: 240px;
    margin-right: 10px;
  }
  .container span {
    margin-left: 10px;
  }
</style>

# CONEX

## Cuprins

- [Intro](#intro)
- [Analiza domeniului](#analiza-domeniului)
- [Tehnologii folosite](#tehnologii-folosite)
- [Utilizarea aplicatiei](#utilizarea-aplicatiei)
- [Proiectarea sistemului](#proiectarea-sistemului)
- [Implementarea functionalitatilor](#implementarea-functionalitatilor)
- [Concluzii](#concluzii)

<hr/>

## **Intro**

Te trezesti intr-o dimineata si nu ai nimic de facut. Un meci de LOL sau o partida de CSGO ar merge de minune. Aprinzi calculatorul, deschizi jocul si... realizezi ca esti singur in echipa si esti nevoit sa intri cu nu stiu cati jucatori la intamplare. Unii poate sunt mahmuri dupa o petrecere, altii poate au o zi mai proasta, incercand sa isi inece amarul in joaca, cativa poate chiar au mii si mii de ore de experienta si sunt priceputi in "arta jucarii". Toate acestea sunt doar niste posibilitati. Poate o sa ai un meci cu coechipieri de treaba, poate nu. Poate o sa te distrezi si ziua o sa iti mearga perfect sau poate un coechipier nenorocit o sa te tradeze si o sa te injunghie pe la spate.

Toate aceste "poate" pot fi eliminate, in totalitate sau partial, folosind **CONEX**, aplicatia care se presupune sa te ajute sa iti gasesti parteneri de joaca, alaturi de care sa te distrezi, nimicind tot ce va sta in cale, sau sa iti tocesti nervii si sa iti strici prieteniile pentru a obtine acel kill mult dorit, menit sa reflecte adevaratele tale reflexe superioare si aptitudini dobandite de-a lungul anilor, pentru ca stim cu totii ca nu conteaza daca castigi sau pierzi, conteaza sa arati bine cand o faci.

## **Analiza domeniului**

### Aplicatii asemanatoare deja existente

|   Caracteristici, cifre si functionalitati   |         Plink         |     GamerLink LFG     |         Conex         |
| -------------------------------------------- |    :-------------:    |    :-------------:    |  :-----------------:  |
| Sistem de operare                            |        Android        |        Android        |        Android        |
| Magazin de aplicatii                         |      Google play      |      Google play      |           ❌          |
| Nota                                         |          1.5          |          4.4          |           ❌          |
| Numar reviewuri                              |       ~ 101.000       |       ~ 10.000        |           ❌          |
| Numar instalari                              |          +1M          |         +500k         |           ❌          |
|                                              |                       |                       |                        |
| Afisare lista jocuri compatibile             |          ✔️           |         ✔️           |           ✔️          |
| Afisare echipe pentru un anumit joc          |          ✔️           |         ✔️           |           ✔️          |
| Postare anunt cautare echipa                 |          ✔️           |         ✔️           |           ✔️          |
| Intrare/iesire/stergere (din) echipa         |          ✔️           |         ✔️           |           ✔️          |
| Adaugare jocuri noi                          |          ❓            |         ❓           |          ✔️          |
| Chat cu echipa                               |           ✔️          |         ✔️           |           ❌          |
| Detalii performanta jucator                  |           ✔️          |         ❌           |           ❌          |
| Limba romana                                 |           ❌          |         ❌           |          ✔️          |
| Beneficii la abonament contra-cost           |          ✔️✔️           |        ✔️            |           ❌          |

Pe piata exista deja cateva aplicatii asemanatoare care, in principiu, fac cam acelasi lucru, poate chiar mai mult.

**Plink** si **GamerLink LFG** sunt 2 exemple, fiind printre cele mai descarcate aplicatii de tipul LFG, specializate pe gaming. Au tot felul de briz-brizuri: poti selecta nivelul de experienta pe care il ai intr-un joc, regiunea in care te joci, modul de joc (casual, competitiv), daca doresti voice chat, vezi detalii despre jucatori etc.

Cu toate aceastea, pentru un utilizator novice, experienta mi s-a parut greoaie. Este destul de complicat si enervant sa trebuiasca sa ai mult de navigat, sa treci prin cateva pagini, pana ajungi unde ai nevoie, cu atat mai mult cand interfata e destul de aglomerata si ce doresti sa faci e cam scopul principal al aplicatiei.

Sa nu fiu inteles gresit, aceste aplicatii si altele sunt mult mai complexe ca si Conex, dar pana la urma folosesti telefonul doar ca pe un mijloc de informare pe care il poti avea mereu la tine. Consider ca functionalitati de acest tip implementate pentru platforme mobile trebuie sa vina ca o extensie a calculatorului. Pana la urma, acolo se intampla actiunea principala.

Conex este gandita destul de simplist. De ce sa intrii in detalii si sa complici unde nu este nevoie? Oamenii vor sa isi caute coechipieri si sa stie cand acestia sunt gasiti. Restul ramane la latitudinea lor.

Astfel, odata creat un cont, esti intampinat cu lista echipelor in cautare de membrii. Tot pe aceasta pagina poti filtra echipele in functie de jocul dorit. Daca te joci LOL nu prea te intereseaza de CSGO. De aici poti sa mergi pe formularul de cautare echipa sau poti sa vezi jocurile momentan compatibile.

Formularul de cautare echipa e destul de simplu: un nume, un joc, un numar de jucatori de care ai nevoie, o descriere optionala, in caz ca vrei sa adaugi ceva detalii referitoare la ce cauti si o chestie interesanta, un masurator de disperare: apesi butonul, zgaltai telefonul si baaaam! daca esti suficient de panicat, entuziasmat sau disperat, culoarea acestuia trece de la verde (chill, avem timp), la galben (as avea putin nevoie de niste oameni, nu vreau sa mint) si, intr-un final, la rosu (TOTI OAMENII SA APELEZE LA MINE!!!). Odata completat poti lansa cu succes anuntul ca iti cauti echipa.

La partea de jocuri, lucrurile raman la fel de simple. Poti sa vezi toate jocurile momentan existente si le poti sterge, daca ai privilegiul de admin. Poti adauga un joc in caz ca ce doresti sa te joci nu exista si ulterior cauta o echipa pentru el.

Intregul "plimbat" intre ecrane se realizeaza printr-un meniu super accesibil, zic eu, aflat in dreapta jos sau dand click pe diverse elemente intuitive.

Aplicatia isi propune sa faca putine lucruri, dar sa le faca cat mai bine si cat mai usor cu putinta. Este menita doar pentru comunitatea romaneasca de jucatori si nu are ca scop crearea unui loc de chat-uit (pentru asta majoritatea foloseste Discord deja).

## **Tehnologii folosite**

### Backend - Firebase

- Baza de date - Firebase Firestore

### Frontend - Flutter (Dart)

### Sistem de versionare - Git

- remote repository - <https://bitbucket.org/drgov_/conexv2/src/main/>

## **Utilizarea aplicatiei**

Navigarea in aplicatie se face prin butoanele din dreapta jos.

### Logare

<div class="container">
  <img src="./docs/imgs/login_screen.png" />
  <span>
    Acesta este ecranul "principal" daca nu esti logat. Introduci datele contului tau si intri in "miezul" aplicatiei.<br/><br/>
    Daca nu ai inca un cont, butonul din dreapta jos te va duce la ecranul de inregistrare.
  </span>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>

### Inregistrare

<div class="container">
  <img src="./docs/imgs/register_screen.png" />
  <span>
    Aici poti sa iti creezi un cont. <br/><br/>
    Odata ce introduci un email, un username si o parola, iar acestea sunt **VALIDE**, vei fi automat logat cu datele introduse si redirectat la pagina cu lista echipelor existente. Daca nu, primesti mesaje destul de sugestive care sa te ajute sa intelegi ce nu e in regula.
  </span>
</div>

### Setari

<div class="container">
  <img src="./docs/imgs/settings_screen.png" />
  <span>
    Momentan de aici poti doar sa iesi din cont, fiind redirectat la ecranul de logare. <br/><br/>
    In principiu aici gasesti cam orice tine de setarile generale ale aplicatiei.
  </span>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/>

### Lista echipelor

<div class="container">
  <img src="./docs/imgs/teams_home_screen.png" />
  <span>
    Aceasta este ecranul "principal" in cazul in care esti logat.<br/><br/>
    De aici poti da click pe o echipa pentru a vedea detalii despre aceasta sau poti filtra echipele in functie de joc.<br/><br/>
    Poti naviga catre:
    <ul>
      <li>Setari</li>
      <li>Lista jocuri</li>
      <li>Adauga echipa</li>
    </ul>
  </span>
</div>

### Adaugarea unei echipe

<div class="container">
  <img src="./docs/imgs/add_team_screen.png" />
  <span>
    Aceasta este pagina de unde poti trimite zvon in tara ca ai nevoie de oameni pentru a supravietui unei sesiuni de joaca.<br/><br/>
    Odata ce completezi campurile, care, sugestiv, sugereaza cu ce ar trebui completate, vestea ca te afli in cautarea unor amici cu care sa iti domini adversarii este lansata.<br/><br/>
    Aici e si cea mai smechera parte a aplicatiei. Ca sa demonstrezi ca esti demn sa gasesti o echipa, apasa pe "Seteaza disperarea" si dovedeste cat de disperat esti agitand telefonul in toate directiile.<br/><br/>
    Apoi trebuie doar sa speri ca se va gasi cineva interesat.<br/><br/>
    Poti naviga catre:
    <ul>
      <li>Setari</li>
    </ul>
  </span>
</div>

<br/><br/>

### Lista jocurilor

<div class="container">
  <img src="./docs/imgs/games_home_screen.png" />
  <span>
    Aici poti vedea o lista cu toate jocurile existente in momentul de fata in aplicatie si poti sterge un joc si, implicit, toate echipele corespunzatoare.<br/><br/>
    Poti naviga catre:
    <ul>
      <li>Setari</li>
      <li>Adauga joc</li>
    </ul>
  </span>
</div>

### Adaugarea unui joc

<div class="container">
  <img src="./docs/imgs/add_game_screen.png" />
  <span>
    Aceasta este pagina de unde poti sa le faci pe plac altora.<br/><br/>
    Aici poti adauga un joc si sa cuceresti comunitatea, devenind cel care a adaugat maretul joc la care toata lumea are nevoie de o echipa.<br/><br/>
    Poti naviga catre:
    <ul>
      <li>Setari</li>
    </ul>
  </span>
</div>

<br/><br/>

## **Proiectarea sistemului**

### Diagrama UML de clase

![uml class diagram](./docs/imgs/uml-class.png)

### Diagrama UML a cazurilor de utilizare

![uml use case](./docs/imgs/uml-use-case.png)

<br/><br/><br/><br/>

## **Implementarea functionalitatilor**

### Cautarea unei echipe

Logica managementului echipelor este adunata intr-o clasa, `TeamsStateNotifier`. Deoarece metodele acestei clase vor fi apelate din UI, pentru a actualiza baza de date in urma diverselor interactiuni, ea o sa detina o instanta de `TeamService`, clasa care contine metode de manipulare a datelor ce tin de echipe.

```dart
class TeamsStateNotifier extends StateNotifier<TeamsState> {
  final TeamService _teamService;

  TeamsStateNotifier(this._teamService) : super(const TeamsInit());
```

`TeamsStateNotifier` expune o variabila care, la un moment dat, se poate afla intr-o stare: initiala - `TeamsInit`, incarcare - `TeamsLoading`, succes - `TeamsSuccess`, eroare - `TeamsError`. Orice actiune asincrona (http request, database fetch) trece aceasta variabila prin loading si apoi succes/eroare, in functie de caz. Aceste stari sunt folosite pentru a lua actiuni corespunzatoare in UI.

```dart
  Future<void> addTeam(Team team) async {
    try {
      state = const TeamsLoading();

      await _teamService.addTeam(team);

      state = TeamsSuccess(AppSuccess('Echipa ${team.name} cauta jucatori'));
    } on AppError catch (error) {
      state = TeamsError(error);
    }
  }
```

Pentru a adauga o echipa vom apela metoda `addTeam(team)` definita in `TeamService`:

```dart
  Future<String> addTeam(Team team) async {
    Validator.validateTeamData(team);
    await _databaseService.add(team);
    return team.id;
  }
```

`TeamService.addTeam(team)` contine o instanta pentru manipularea bazei de date de tip `DatabaseService<Team>`. In momentul in care se doreste adaugarea unei echipe se parcurg urmatorii pasi: se valideaza datele echipei folosind niste restrictii presetate, iar daca totul e ok si datele sunt valide se apeleaza metoda de adaugare in baza de date.

```dart
  static void validateTeamData(Team team) {
    if (empty(team.name)) {
      throw InvalidError('Nume echipa nu e completat');
    }
    if (empty(team.neededPlayers)) {
      throw InvalidError('Numar jucatori necesar nu e completat');
    }
    if (notNumber(team.neededPlayers)) {
      throw InvalidError('Numar jucatori nu e numar');
    }
    if (empty(team.gameId)) {
      throw InvalidError('Nu ai ales niciun joc');
    }
  }
```

Pentru ca o echipa sa fie valida, aceasta trebuie sa nu aiba un nume gol, numarul de jucatori necesar trebuie sa nu fie gol si sa fie un numar, evident... si un joc trebuie sa fie selectat. In caz contrat, se arunca o eroare de tip `InvalidError(message) extends AppError`.

Pentru cautarea unei echipe userul va trebui sa completeze un formular format din: nume echipa, joc, numar jucatori necesar, descriere (optionala), setare grad de disperare (optional, prin scuturarea telefonului). Pentru construirea formularului si stocarea informatiilor din UI vom folosi un widget `Stateful` si `State`-ul asociat lui. Acest widget contine cate un camp pentru completarea fiecarei informatii si variabile la care conectam valorile din input-uri:

```dart
class _FormState {
  final Game? game;
  final bool showTip;
  final String severity;

  const _FormState({
    this.game,
    this.showTip = false,
    this.severity = 'low',
  });

  _FormState reset() => const _FormState();
  _FormState setGame(Game? game) =>
      _FormState(game: game, showTip: showTip, severity: severity);
  _FormState setSeverity(String severity) =>
      _FormState(game: game, showTip: showTip, severity: severity);
  _FormState setShowTip(bool showTip) =>
      _FormState(game: game, showTip: showTip, severity: severity);
  _FormState showTipResetSeverity() => setShowTip(true).setSeverity('low');
  _FormState hideTip() => setShowTip(false);
}
```

```dart
final nameController = TextEditingController();
final neededPlayersController = TextEditingController();
final descriptionController = TextEditingController();
```

```dart
CstmTextField(
    controller: nameController,
    hintText: 'Nume echipa',
    margin: const EdgeInsets.only(bottom: Spacing.m),
),
CstmDropDownField<Game>(
    value: formState.game,
    item: (game) => CstmText(game.name),
    items: games,
    onChanged: (value) =>
        notifier.update((state) => state.setGame(value)),
    hintText: 'Joc',
    margin: const EdgeInsets.only(bottom: Spacing.m),
),
NumberTextField(
    controller: neededPlayersController,
    hintText: 'Numar jucatori',
    margin: const EdgeInsets.only(bottom: Spacing.m),
),
CstmTextField(
    controller: descriptionController,
    hintText: 'Descriere',
    margin: const EdgeInsets.only(bottom: Spacing.l),
),
```

Pentru implementarea functionalitatii de setare a disperarii prin scuturarea telefonului am utilizat un buton care apeleaza o functie, `shake()`. Aceasta functie "asculta" timp de o durata presetata accelerometrul telefonul si in functie de niste parametrii determina numarul de scuturari si gradul de disperare echivalent.

```dart
Btn(
    onPressed: shake,
    child: const Text('Seteaza disperarea'),
    margin: const EdgeInsets.only(bottom: Spacing.s),
),
```

```dart
  Future<void> shake() async {
    final notifier = ref.read(_formStateProvider.notifier);
    notifier.update((state) => state.showTipResetSeverity());
    var lastShake = 0;
    var shakesCount = 0;
    accelerometerSub = userAccelerometerEvents.listen((event) {
      final now = DateTime.now().millisecondsSinceEpoch;
      if (now - lastShake > _MIN_TIME_BETWEEN_SHAKES_MS) {
        if (getAccel(event) > _SHAKE_THRESHOLD) {
          lastShake = now;
          shakesCount++;
        }
        if (_LOWER_LIMIT < shakesCount && shakesCount < _UPPER_LIMIT) {
          notifier.update((state) => state.setSeverity('medium'));
        }
        if (shakesCount >= _UPPER_LIMIT) {
          notifier.update((state) => state.setSeverity('high'));
        }
      }
    });
    await Future.delayed(const Duration(seconds: _LISTEN_TIME), () {
      notifier.update((state) => state.hideTip());
      accelerometerSub!.cancel();
    });
  }
```

Cand se doreste adaugarea echipei se foloseste un buton care apeleaza metoda din `TeamsStateNotifier` descrisa la inceput.

```dart
Btn(
    onPressed: createTeam,
    child: const Text('Cauta jucatori'),
)

  Future<void> createTeam() async {
    final loggedUser = ref.watch(loggedUserProvider).value!;
    final addTeamFormState = ref.watch(_formStateProvider);
    final team = Team(
      name: nameController.text.trim(),
      neededPlayers: neededPlayersController.text.trim(),
      description: descriptionController.text.trim(),
      severity: addTeamFormState.severity,
      gameId: addTeamFormState.game?.id ?? '',
      userId: loggedUser.id,
    );
    await ref.read(addTeamProvider.notifier).addTeam(team);
  }
```

## **Concluzii**

Te trezesti intr-o dimineata si nu ai nimic de facut. Un meci de LOL sau o partida de CSGO ar merge de minune. Aprinzi calculatorul -

Parca am mai mentionat undeva asta... A, da, in introducere. Hai sa nu mai repetam.

Ideea acestei aplicatii a venit datorita, sau din cauza, depinde cum o iei, propriei experiente.

Candva, in vremurile indepartate, am fost un partizan acerb al jocurilor competitive, al jucatului pentru clasament si pentru rang, pentru niste numere pe ecran care aparent imi dovedeau abilitatile. Pierdeam zile si nopti incercand sa mai castig catusi de putin, sa ies mereu pe plus, sa urc tot mai sus in ierarhie. Nu pot sa fiu bronze, e imposibil, sunt prea bun pentru bronze. Ajungi silver si zici acelasi lucru, apoi gold, platinum si tot asa. Niciodata nu eram multumit, dar nu asta e relevant aici.

Cand intram in joc, de regula, eram singur si pus in echipa cu oameni la intamplare, fie care se stiau intre ei sau nu. Multi nu stiau engleza sau vorbeau in limba lor, intelegandu-se doar intre ei.

E frustrant cand tu te lupti sa castigi, dar astfel de impedimente apar fara sa vrei. In jocurile multiplayer conteaza foarte mult comunicarea dintre membrii echipei, iar daca ei nu pot comunica, ei bine... sa zicem doar ca ai din start un handicap considerabil. Vrei sa simti acea tensiune cand te chinui si rezultatul meciului este obtinut in mod cinstit. Nu vrei sa pierzi doar pentru ca cineva refuza sau nu poate coopera.

Conex se presupune sa te ajute sa eviti pe cat posibil astfel de lucruri. Sa poti sa iti cunosti din avans cat de cat tovarasii de joaca. Sa poti sa stai linistit pentru ca stii ca atunci cand vrei sa te joci, ai o echipa care te asteapta. Sa ai o experienta mai placuta cand vrei fie sa te relaxezi, fie sa transpiri jucandu-te ceva.

Cred ca este evident ca probabil aceasta aplicatie nu este o solutie care le rezolva pe toate. Nu garanteaza nimeni ca cei din echipa nu vor fii niste nesuferiti. Factorul uman determina intr-un procent foarte mare buna functionare a aplicatiei si multe lucruri pot merge prost, dar, momentan, ne punem increderea in utilizatori.

Acum, cateva functionalitati si lucruri pe care planuiesc sa le adaug:

- optiune de actualizare a unei echipe, joc, user
- notificare atunci cand o echipa in care te afli s-a umplut
- setarea datei in care ai nevoie de o echipa
- date reprezentative pentru un anumit joc pentru fiecare utilizator (numar ore jucate, rang competitiv, main role etc.)
- stergerea oricarei echipe exclusiv pentru admini
- mai multe filtre (dupa gradul de disperare, dupa data adaugata, dupa rang)
- orice altceva ce apare pe parcurs

Pe final, as dori sa mentionez ca ceea ce am reusit sa fac se afla, intr-o proportie destul de mare, in concordanta cu ce mi-am imaginat si planuit.

Stiu ca poate nu conteaza, dar am decis sa dezvolt aplicatia folosind un limbaj cu care nu eram tocmai familiar si confortabil si cu care acum m-am confruntat pentru prima oara. Pe langa faptul ca am reusit sa fac ca o mare parte din ideile mele sa prinda o forma, mai mult sau mai putin frumoasa, am dobandit si niste aptitudini si am invatat lucruri noi.

 Daca ar fi sa ma gandesc, pe o scara de la *"Nu instalez asa ceva pe telefon"* la *"DAAAA! ACUM O INSTALEZ. CUM II ZICE?"*, m-as situa undeva pe la mijloc, adica *"Hai ca o instalez sa vad si eu despre ce e vorba"*.

 Pentru ce isi propune sa faca a, nota ~~deloc~~ subiectiva acordata de mine aplicatiei e **4/5**.
