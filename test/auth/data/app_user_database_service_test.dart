import 'package:conex/auth/data/app_user.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../data.dart';

void main() {
  final collectionRef = instance.collection('users');

  setUp(() async {
    await createUsersSample();
  });

  test(
      'GIVEN [a user id] WHEN [getUserById(String id)] DO [return an AppUser stream]',
      () async {
    final user =
        AppUser(id: 'abc', email: 'email@gmail.com', username: 'username');

    await collectionRef.doc(user.id).set(user.toMap());
    final result = await appUserDatabaseService.getUserById(user.id).first;

    expect(result, user);
  });

  test(
      'GIVEN [a list of users ids] WHEN [getUsersByIds(List<String> usersIds)] DO [return a List<AppUser> stream]',
      () async {
    final usersIds = [user1.id, user2.id];

    final result = await appUserDatabaseService.getUsersByIds(usersIds).first;

    expect(result.contains(user1), true);
    expect(result.contains(user2), true);
    expect(result.contains(user3), false);
  });

  test(
      'GIVEN [a user object] WHEN [postUser(AppUser user)] DO [add the user in database]',
      () async {
    final user = AppUser(
      id: 'abc',
      email: 'email@gmail.com',
      username: 'username',
    );

    final resultId = await appUserDatabaseService.addUser(user);
    final result =
        AppUser.fromMap((await collectionRef.doc(user.id).get()).data()!);

    expect(resultId, user.id);
    expect(result, user);
  });

  test(
      'GIVEN [a user id] WHEN [deleteUserById(String id)] DO [delete the user from database]',
      () async {
    await appUserDatabaseService.deleteUserById(user1.id);
    final users = await appUserDatabaseService.getUsers().first;

    expect(users.contains(user1), false);
    expect(users.contains(user2), true);
  });
}
