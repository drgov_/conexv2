import 'package:conex/game/data/_.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../data.dart';

void main() {
  final collectionRef = instance.collection('games');

  setUp(() async {
    await createGamesSample();
  });

  test(
      'GIVEN [-] WHEN [getGames()] DO [return a List<Game> stream of all games]',
      () async {
    final result = await gamesDatabaseService.getGames().first;

    expect(result.contains(game1), true);
    expect(result.contains(game2), true);
    expect(result.contains(game3), false);
  });

  test(
      'GIVEN [a game id] WHEN [getGameById(String id)] DO [return a Game stream]',
      () async {
    final game = game1;

    final result = await gamesDatabaseService.getGameById(game.id).first;

    expect(result, game);
    expect(result != game2, true);
  });

  test(
      'GIVEN [a game name] WHEN [getGameByName(String name)] DO [return a Game stream]',
      () async {
    final game = game1;

    final result = await gamesDatabaseService.getGameByName(game.name).first;

    expect(result, game);
    expect(result != game2, true);
  });

  test(
      'GIVEN [a game object] WHEN [createGame(Game game)] DO [add game in database]',
      () async {
    final game = Game(id: 'abc', name: 'gameabc', maxTeam: 3);

    final resultId = await gamesDatabaseService.addGame(game);
    final result =
        Game.fromMap((await collectionRef.doc(game.id).get()).data()!);

    expect(resultId, game.id);
    expect(result, game);
  });

  test(
      'GIVEN [a game id] WHEN [deleteGameById(String id)] DO [delete game from database]',
      () async {
    final game = game1;

    await gamesDatabaseService.deleteGameById(game.id);
    final games = await gamesDatabaseService.getGames().first;

    expect(games.contains(game1), false);
    expect(games.contains(game2), true);
  });
}
