import 'package:conex/auth/data/_.dart';
import 'package:conex/common/database/_.dart';
import 'package:conex/game/data/_.dart';
import 'package:conex/team/data/_.dart';
import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';

final instance = FakeFirebaseFirestore();

final appUserDatabaseService = AppUserService(FStoreService(instance, 'users'));
final gamesDatabaseService =
    GameService(FStoreService(instance, 'games'), teamsDatabaseService);
final teamsDatabaseService = TeamService(FStoreService(instance, 'teams'));
final detailedTeamsDatabaseService = TeamGameUsersService(
    teamsDatabaseService, gamesDatabaseService, appUserDatabaseService);

final user1 =
    AppUser(id: '1', email: 'email1@gmail.com', username: 'username1');
final user2 =
    AppUser(id: '2', email: 'email2@gmail.com', username: 'username2');
final user3 =
    AppUser(id: '3', email: 'email3@gmail.com', username: 'username3');

final game1 = Game(id: '1', name: 'game1', maxTeam: 3);
final game2 = Game(id: '2', name: 'game2', maxTeam: 5);
final game3 = Game(id: '3', name: 'game3', maxTeam: 2);

final team1 = Team(
    id: '1', name: 'team1', neededPlayersNumber: 4, gameId: '1', userId: '2');
final team2 = Team(
    id: '2', name: 'team2', neededPlayersNumber: 5, gameId: '2', userId: '1');
final team3 = Team(
    id: '3', name: 'team3', neededPlayersNumber: 3, gameId: '1', userId: '1');
final team4 = Team(
    id: '4', name: 'team4', neededPlayersNumber: 8, gameId: '2', userId: '1');

Future<void> createUsersSample() async {
  await instance.collection('users').doc(user1.id).set(user1.toMap());
  await instance.collection('users').doc(user2.id).set(user2.toMap());
}

Future<void> createGamesSample() async {
  await instance.collection('games').doc(game1.id).set(game1.toMap());
  await instance.collection('games').doc(game2.id).set(game2.toMap());
}

Future<void> createTeamsSample() async {
  await instance.collection('teams').doc(team1.id).set(team1.toMap());
  await instance.collection('teams').doc(team2.id).set(team2.toMap());
}
