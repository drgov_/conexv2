import 'package:flutter_test/flutter_test.dart';

import '../../data.dart';

void main() {
  setUp(() async {
    await createUsersSample();
    await createGamesSample();
    await createTeamsSample();
  });

  test('TEST', () async {
    final team = team1;

    final result = await detailedTeamsDatabaseService
        .getTeamGameUsersByTeamId(team.id)
        .first;
    final teamGame = await gamesDatabaseService.getGameById(team.gameId).first;
    final teamUsers =
        await appUserDatabaseService.getUsersByIds(team.usersIds).first;

    expect(result.team, team);
    expect(result.game, teamGame);
    expect(result.users, teamUsers);
  });

  test('TEST2', () async {
    final result =
        await detailedTeamsDatabaseService.getTeamsGamesUsers().first;
  });
}
